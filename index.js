
const fs = require("fs");
const path = require("path");

const express = require("express");

const ffmpeg = require('fluent-ffmpeg');

/**
 * Main class for the whole library.
 * @class CMSGallery 
 * @hideconstructor
 */
module.exports = class CMSGallery {
  constructor(categ_table) {
    this.categ_table = categ_table;

  }

  /**
   * @static
   * @async
   * @method construct
   * @memberof CMSGallery 
   * @arg {Mellisuga} cms CMS object
   * @arg {Object} cfg Configuration 
   * @returns {CMSGallery}
   */
  static async init(cms, cfg) {
    console.log("+", "Gallery");
    let categ_table = await cms.aura.table('album_categories', {
      columns: {
        id: 'uuid',
        name: 'text'
      }
    });

    let _this = new module.exports(categ_table);


    let album_table = _this.album_table = await cms.aura.table("albums", {
      columns: {
        id: 'SERIAL PRIMARY KEY',
        title: 'text',
        description: 'text',
        cover: 'text',
        categories: 'text[]'
      }
    });

    let page = cms.cms.serve_extra_page("Gallery", path.resolve(__dirname, 'dist', 'Gallery'), {
      add_to_menu: true
    });
    let aedit_page = cms.cms.serve_extra_page("AlbumEditor", path.resolve(__dirname, 'dist', 'AlbumEditor'));
    const gallery_path = path.resolve(cms.app_path, "gallery");
/*
    let default_album = await album_table.select("*", "id = $1", [1]);

    let default_path = path.resolve(gallery_path, "Default");
    if (!fs.existsSync(default_path)) {
      fs.mkdirSync(default_path);
    }


    let default_id = 1;
    if (default_album.length < 1) {
      default_id = await album_table.insert({
        title: "Default",
        description: "This is the default album"
      });
    }

    console.log("default id", default_id);
*/
    let table = await cms.aura.table('media', {
      columns: {
        id: 'serial',
        index: 'bigint',
        title: 'text',
        text: 'text',
        name: 'text',
        album: 'integer REFERENCES albums(id)',
        hidden: 'boolean',
        json_data: 'jsonb'
      }
    });
    const path_prefix = "/content-manager";
    let app = cms.app;


    if (!fs.existsSync(gallery_path)){
      fs.mkdirSync(gallery_path);
    }

    let db_albums = await album_table.select(['title', 'id']);
    for (let a = 0; a < db_albums.length; a++) {
      let db_srcs = await table.select(['name'], "album = $1", [db_albums[a].id]);
      for (let s = 0; s < db_srcs.length; s++) {
        if (!fs.existsSync(path.resolve(gallery_path, db_albums[a].title, db_srcs[s].name))){
          await table.delete("name = $1", [db_srcs[s].name]);
        }
      }
    }

    async function load_album(apath, atitle, aid) {
      try {
        let media_files = fs.readdirSync(apath);
/*
        media_files.sort(function(a, b) {
          return fs.statSync(apath+'/'+a).mtime - fs.statSync(apath+'/'+b).mtime
        });
  */      
        media_files.sort(function(a, b) {
          var nameA = a.toUpperCase(); // ignore upper and lowercase
          var nameB = b.toUpperCase(); // ignore upper and lowercase
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }

          // names must be equal
          return 0;
        });
        for (let f = 0; f < media_files.length; f++) {
          let file = media_files[f];
          if (!file.endsWith('.webm')) {

            let mmax =(await table.query("select index from media WHERE album = "+aid+" order by index desc limit 1")).rows;
            let max_id = 1;
            if (mmax[0]) max_id = parseInt(mmax[0].index)+1;

            const exists = await table.select('name', "name = $1 AND album = $2", [file, aid]);
            if (exists.length < 1) {
              await table.insert({
                index: max_id,
                name: file,
                album: aid
              });
            }
          }
        }

        await reindex_media(aid);
      } catch (e) {
        console.log(e.stack);
      }
    }

    let album_dirs = fs.readdirSync(gallery_path);

    if (!cfg.disable_start_indexing) {
      for (let d = 0; d < album_dirs.length; d++) {
        let existing = await album_table.select(["id"], "title = $1", [album_dirs[d]]);
        if (existing.length < 1) {
          let nalbum = await album_table.insert({
            title: album_dirs[d]
          });
          existing.push({ id: nalbum });
        }
     //   console.log("exists", existing[0]);
        if (!fs.existsSync(gallery_path)) fs.mkdirSync(gallery_path);
        await load_album(gallery_path+"/"+album_dirs[d], album_dirs[d], existing[0].id);
      }
    }

    this.path = gallery_path;

    console.log(" GALLERY PATH PREFIX", path_prefix);

    app.get(path_prefix+"/gallery", async function(req, res) {
      try {
        async function get_album(val) {
          if (val) {
            if (typeof val === 'string') {
              let found_albums = await album_table.select(['id'], 'title = $1', [val]);
              if (found_albums.length > 0) {
                val = found_albums[0].id;
              } else {
                return false;
              }
            }

            return val;
          } else {
            return false;
          }
        }

        var data = JSON.parse(req.query.data);

        switch (data.command) {
          case 'all_albums':
            res.json(await album_table.select("*"));
            break;
          case 'all':

            let srcs;
            if (data.album) {
              data.album = await get_album(data.album);
              srcs = await table.select('*', "album = $1", [data.album]);
            } else {
              srcs = await table.select('*');
            }

            srcs.sort(function(a, b) {
              return b.index - a.index;
            });

            res.send(JSON.stringify(srcs));
            break;
          case 'album_by_id':
            res.json((await album_table.select("*", "id = $1", data.id))[0]);
            break;
          case 'random_items':
            let rialbums = data.category ? await album_table.select(
              ["id"], "categories @> $1", [[data.category]]
            ) : false;
            for (let a = 0; a < rialbums.length; a++) {
              rialbums[a] = rialbums[a].id
            }
            let riwstr = data.category ? "ARRAY[album] <@ $1" : false;
            let riargs = data.category ? [rialbums] : [];
            riargs.push(data.limit || 10);
//            console.log(riwstr, riargs);
            let riitems = await table.select("*", riwstr, riargs, {
              limit: "$2",
              order_by: "random()"
            });
            for (let i = 0; i < riitems.length; i++) {
              riitems[i].album = (await album_table.select("*", "id = $1", [riitems[i].album]))[0].title;
            }
            res.json(riitems);
            break;
          case 'page':
        //    let wstr = 'index >= $1 AND index <= $2';
        //    let warg = [data.min, data.max];
            let wstr = "";
            let warg = [data.offset, data.limit];
            if (data.album) {
              data.album = await get_album(data.album);
              wstr += 'album = $3';
              if (!data.show_hidden) {
                wstr += ' AND (hidden = false OR hidden IS NULL)';
              }
              warg.push(data.album);
            } else if (!data.show_hidden) {
              wstr += 'hidden = false OR hidden IS NULL';
            }
            let media_items = await table.select('*', wstr, warg, {
              offset: "$1",
              limit: "$2",
              order_by: "index"
            });

            media_items.sort(function(a, b) {
              return b.index - a.index;
            });

       //     console.log(media_items);
            res.send(JSON.stringify(media_items));
            break;
          case 'max_index':
            let miqstr = 'select count(*) AS exact_count FROM media';
            if (data.album) {
              data.album = await get_album(data.album);
              if (data.album) {
                miqstr += ' WHERE album = '+data.album;
              }
              if (!data.show_hidden) {
                miqstr += 'AND (hidden = false OR hidden IS NULL)';
              }
            } else if (!data.show_hidden) {
              miqstr += ' WHERE hidden = false OR hidden IS NULL';
            }
            const max_indexa = (await table.query(miqstr)).rows[0].exact_count;
          //  console.log("MAX INDEX ", max_indexa, "album", data.album);
            res.json(parseInt(max_indexa));
            break;
          case 'all_categories':
            var list = await _this.all_categories();
            res.json(list);
            break;
          case 'get_albums_by_category':
            res.json(await _this.get_albums_by_category(data.category));
            break;
          default:

        }

      } catch (e) {
        console.error(e);
      }
      
    });

    async function reindex_media(aid) {
     // let ids = (await table.query("select id,index from media where album = "+aid+" order by index")).rows;
      let ids = await table.select(['id', 'index'], "album = $1", [aid]);

      ids.sort(function(a, b) {
        return a.index - b.index;
      });

      for (let i = 0; i < ids.length; i++) {
      //    console.log(ids[i].id, "nindex", i+1);
        if (ids[i].index != i+1) {
          await table.update({
            index: i+1
          }, "id = $1", [ids[i].id]);
    //      console.log("updated");
        };
      }
    }
    
    const rmrf = function(dpath) {
      if (fs.existsSync(dpath)) {
        fs.readdirSync(dpath).forEach((file, index) => {
          const curPath = path.join(dpath, file);
          if (fs.lstatSync(curPath).isDirectory()) { // recurse
            rmrf(curPath);
          } else { // delete file
            fs.unlinkSync(curPath);
          }
        });
        fs.rmdirSync(dpath);
      }
    };


    app.use('/gallery-directory', express.static(gallery_path));

    app.post(path_prefix+"/gallery", cms.admin.auth.orize_gen(["content_management"]), async function(req, res) {
      try {
        var data = JSON.parse(req.body.data);

        switch (data.command) {
          case 'rm':
            let rma = (await album_table.select(["id", "title"], "id = $1", [data.aid]))[0];
            let rmatitle = rma.title;
            for (let i = 0; i < data.ids.length; i++) {
              let rmainame = (await table.select(["name"], "id = $1", [data.ids[i]]))[0].name;
              var image_path = path.resolve(gallery_path, rmatitle, rmainame);
              fs.unlinkSync(image_path); 
              if (image_path.endsWith('.mp4')) {
                fs.unlinkSync(image_path.slice(0, -3)+'webm');
              }
              const id_del = await table.delete("id = $1", [data.ids[i]]);
    //          console.log("File deleted", image_path);
            } 
            await reindex_media(rma.id);
            res.send("success");
            break;
          case 'rm_album':
            let rmamedia = await table.select(["id"], "album = $1", [data.id]);
            for (let m = 0; m < rmamedia.length; m++) {
              await table.delete("id = $1", [rmamedia[m].id])
            }
            let rmaname = (await album_table.select(["title"], "id = $1", [data.id]))[0].title;
            console.log(gallery_path, rmaname);
            rmrf(path.resolve(gallery_path, rmaname));
            await album_table.delete("id = $1", [data.id]);
            res.json({cool: true});
            break;
          case 'new_index':
            const newi = data.new_index;
            const oldi = parseInt(data.old_index);

            if (newi < oldi) {
              let to_reindex = await table.select(["index", "id"], "index >= $1 AND index < $2 AND album = $3", [newi, oldi, data.aid]);
              for (let t = 0; t < to_reindex.length; t++) {
                let tore = to_reindex[t];
                await table.update({ 
                  index: parseInt(tore.index)+1
                }, "id = $1", [tore.id]);
              }
            } else {
              let to_reindex = await table.select(["id", "index"], "index <= $1 AND index > $2 AND album = $3", [newi, oldi, data.aid]);
              for (let t = 0; t < to_reindex.length; t++) {
                let tore = to_reindex[t];
                await table.update({ 
                  index: parseInt(tore.index)-1
                }, "id = $1", [tore.id]);
              }
            }

            await table.update({
              index: newi
            }, "id = $1", [data.id]);

            await reindex_media(data.aid);

            res.send("success");
            break;
          case 'title':
            await table.update({ 
              title: data.title
            }, "id = $1", [data.id]);
            res.send("success");
            break;
          case 'text':
            await table.update({ 
              text: data.text
            }, "id = $1", [data.id]);
            res.send("success");
            break;
          case 'json_edit':
            await table.update({
              hidden: data.hidden,
              json_data: data.json
            }, "id = $1", [data.id]);

            if (data.album_cover) {
              await album_table.update({
                cover: data.album_cover.src
              }, "id = $1", data.album_cover.id);
            }

            res.send("success");
            break;
          case 'new_album':
            if (!data.title) {
              res.send("NO TITLE!");
            } else {
              let napath = path.resolve(gallery_path, data.title);
              if(!fs.existsSync(napath)) fs.mkdirSync(napath);
              res.json(await album_table.insert({
                title: data.title,
                description: data.desc
              }));
            }
            break;
          case 'edit_album':
            let eaopath = path.resolve(gallery_path, (await album_table.select(['title'], "id = $1", [data.aid]))[0].title);
            let eanpath = path.resolve(gallery_path, data.title);
            fs.renameSync(eaopath, eanpath);
            await album_table.update({
              title: data.title,
              description: data.desc,
              categories: data.categ
            }, "id = $1", [data.aid]);

            res.json({ cool: true });
            break;
          case 'add_category':
            if (data.name) {
              var json = await _this.add_category(data.name);
              res.send(json);
            }
            break;
          case 'del_category':
            if (data.name) {
              var json = await _this.del_category(data.name);
              res.send(json);
            }
            break;
          default:

        }
      } catch (e) {
        console.error(e.stack);
      }
    });

    var multer  = require('multer');
    var storage = multer.diskStorage({
      destination: function (req, file, cb) {
        let hparams = JSON.parse(decodeURIComponent(req.header('params')));
        cb(null, path.resolve(gallery_path, hparams.album))
      },
      filename: function (req, file, cb) {
          let nname = file.originalname.replace(/[`~!@#$%^&*()|+\-=?;:'",<>\{\}\[\]\\\/ ]/gi, '_'); //`
          cb(null, nname)
        }
      }
    )

    var upload = multer({ storage: storage })

    app.post(path_prefix+'/gallery-upload', cms.admin.auth.orize_gen(["content_management"]), upload.array('filei'), async function(req, res) {
      try {
        let data = req.files;
        let body = req.body;
        body.album = decodeURIComponent(body.album);
        let hparams = JSON.parse(decodeURIComponent(req.header('params')));
        const album_id = hparams.album_id;
        body.album = album_id;
//        let name_list = [];
        let mmax =(await table.query("select index from media WHERE album = "+body.album+" order by index desc limit 1")).rows; 
        for (var f = 0; f < data.length; f++) {
//          console.log("File uploaded to: ", data[f].path);
          let exists = await table.select(["name"], "name = $1 AND album = $2", [data[f].filename, album_id]);

          if (exists.length < 1) {
            let max_id = 1;
            if (mmax[0]) max_id = parseInt(mmax[0].index)+1+f;
            let new_media = {
              index: max_id,
              name: data[f].filename
            };
            if (body.album) new_media.album = body.album;
            if (body.title) new_media.title = body.title;
            if (body.text) new_media.text = body.text;

            if (data[f].filename.endsWith('.webm')) {
              new_media.name = new_media.name.slice(0, -4)+"mp4";
            }

            await table.insert(new_media);
  //          name_list.push(new_media);

            await new Promise(function(resolve) {
              if (data[f].filename.endsWith(".mp4")) {
                
              //  var command = ffmpeg(path.resolve(gallery_path, data[f].filename))
              //    .audioCodec('libvorbis')
              //    .videoCodec('libvpx')
              //    .format('webm');

                // Create a clone to save a small resized version
              //  command.save(path.resolve(gallery_path, data[f].filename.slice(0, -4)+'.webm'))
              //    .on('end', function() {
                    resolve();
              //    });
              } else if (data[f].filename.endsWith(".webm")) {

                var command = ffmpeg(path.resolve(gallery_path, data[f].filename))
                  .audioCodec('libmp3lame')
                  .videoCodec('libxvid')
                  .format('mp4');

                // Create a clone to save a small resized version
                command.save(path.resolve(gallery_path, data[f].filename.slice(0, -5)+'.mp4'))
                  .on('end', function() {
                    resolve();
                  });
              } else {
                resolve();
              }
            });
          }
        }
        await reindex_media(album_id);

        res.send({});
      } catch (e) {
        console.error(e.stack);
      }
    });
  }

  async all_categories() {
    try {
      var posts = await this.categ_table.select('*');
      return posts;
    } catch (e) {
      console.error(e.stack);
      return false;
    }
  }

  async add_category(name) {
    try {
      var found = await this.categ_table.select("*", "name = $1", [name]);

      var json;
      if (found.length == 0) {
        var result = await this.categ_table.insert({
          name: name
        });
        json = JSON.stringify({
          id: result
        });
      } else {
        json = JSON.stringify({
          err: "Category named `"+name+"` already exists!"
        });
      }
      return json;
    } catch (e) {
      console.error(e.stack);
      return false;
    }

  }

  
  async del_category(name) {
    try {
      await this.categ_table.delete("name = $1", [name]);
      return {};
    } catch (e) {
      console.error(e.stack);
      return false;
    }
  }

  async get_albums_by_category(name) {
    try {
      let albums = this.album_table.select("*", "categories @> $1", [[name]]);
      return albums;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
}
