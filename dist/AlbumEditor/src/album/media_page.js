import XHR from 'core/utils/xhr_async.js';
import Image from './image.js';

import GridUI from 'core/grid.ui/index.js';

let progress_block = document.getElementById("progress_block");
progress_block.progress_percentage = progress_block.querySelector("h3");
progress_block.progress_bar = progress_block.querySelector(".progress_bar");
progress_block.progress_bar.status = progress_block.querySelector("p");
progress_block.progress_bar.fill = progress_block.querySelector("span");

export default class {
  constructor(cfg, navigation) {


    let index = this.index = cfg.index;
    let page_size = this.page_size = cfg.page_size;
    let max_pages = this.max_pages = cfg.max_pages;
    let max_index = this.max_index = cfg.max_index;
    let images = this.images = [];

    this.album = cfg.album;
    this.navigation = navigation;

    let grid_element = this.gride = document.createElement('table');
    navigation.display.appendChild(grid_element);
    
    let grid_ui = this.grid_ui = new GridUI(6, window.innerWidth, 10, 300, grid_element, true);
//    document.body.appendChild(grid_ui.element);
    grid_ui.resize(window.innerWidth);

    let cur_in_row = this.cur_in_row = grid_ui.items_in_row;
    let this_class = this;

    let select_all = document.getElementById("select_all");
    select_all.addEventListener("change", function(e) {
      if (select_all.checked) {
        images = this_class.images;
        for (let i = 0; i < images.length; i++) {
          images[i].select();
          if (!images[i].element.contains(images[i].controls)) {
            images[i].element.appendChild(images[i].controls);
          }
        }
      } else {
        for (let i = 0; i < images.length; i++) {
          images[i].deselect();
          if (images[i].element.contains(images[i].controls)) {
            images[i].element.removeChild(images[i].controls);
          }
        }
      }
    });

    let delete_selection_button = this.delete_selection_button = document.getElementById("delete_selection");
    this.delete_listener = function(ev) {      
      images = this_class.images;

      let selected_items = [];
      let srcs = [];
      for (let i = 0; i < images.length; i++) {
        if (images[i].checkbox.checked) {
          selected_items.push(images[i]);
          srcs.push(images[i].id);
        }
      }

      if (selected_items.length > 0) {
        let popup = document.getElementById('del_popup');
        popup.style.display = "block";

        let ybtn = popup.querySelector(".del_yes");
        let ylisten = async (ev0) => {
          try {
            popup.style.display = "";
            display_loading_overlay();
            console.log(srcs);
            let resp = await XHR.post("/content-manager/gallery", {
              command: "rm",
              aid: this_class.album.id,
              ids: srcs
            }, "access_token");
            if (resp === "success") {
              ybtn.removeEventListener("click", ylisten);
              let ilen_befdel = images.length; 
              for (let i = 0; i < selected_items.length; i++) {
                grid_ui.remove(selected_items[i].element);
                images.splice(images.indexOf(selected_items[i]), 1);
              }
              
              select_all.checked = false;


              if (selected_items.length == ilen_befdel && index > 0) { 
                await navigation.get_page(index-1);
              } else {
                await this_class.resize(this_class.grid_ui.items_in_row);
              }
            } else {
              window.location.reload();
            }
            hide_loading_overlay();
          } catch (e) {
            console.error(e.stack);
          }
        };
        ybtn.addEventListener("click", ylisten);

        let nbtn = popup.querySelector(".del_no");
        let nlisten = (e) => {
          nbtn.removeEventListener("click", nlisten);
          popup.style.display = "";
        };
        nbtn.addEventListener("click", nlisten);
      }  
    }
    this.delete_selection_button.addEventListener('click', this.delete_listener);
  }

  static async init(index, page_size, max_index, navigation, album_id) {
    try {
      let this_class = new module.exports({
        index: index,
        page_size: page_size,
        max_index: max_index,
        album: album_id,
        show_hidden: true
      }, navigation);
      var videos = document.getElementsByTagName("video");

      window.addEventListener('scroll', function(ev) {
        this_class.checkScroll
      }, false);
      return this_class;
    } catch (e) {
      console.error(e);
      return undefined;
    }
     
  }

  listen_resize() {
    let this_class = this;

    this.resize_listener = async function(ev) {
      try {
        if (!this_class.resizing) {
          console.log(this_class);
          console.log("resizing", Date.now(), this_class.resizing);
          this_class.resizing = true;
          console.log("resizing state now", this_class.resizing);
          this_class.grid_ui.resize(window.innerWidth);
          let inrow = this_class.grid_ui.items_in_row;
          if (inrow != this_class.cur_in_row) {
            this_class.destroyed = true;
            await this_class.resize(inrow);
            this_class.cur_in_row = inrow;
          }
          this_class.checkScroll();

          this_class.resizing = false;
          console.log("resizing state now", this_class.resizing);
          if (this_class.resize_again) {
            console.log("resize again");
            this_class.resize_again = false;
            await this_class.resize_listener();
          }
          console.log("end");
        } else {
          this_class.resize_again = true;
        }
      } catch(e) {
        console.error(e.stack);
      } 
    };
    window.addEventListener('resize', this.resize_listener);

  }


  async resize(inrow) {
    try {
      console.log("resize");
      if (!inrow) inrow = this.cur_in_row; else this.cur_in_row = inrow;
      this.images.forEach(function(imag) {
        imag.destroy();
      });
      this.grid_ui.remove_all();
      this.images = [];
  //    document.body.appendChild(this.grid_ui.element);

      this.max_index = await XHR.get('/content-manager/gallery', {
        command: "max_index",
        album: this.album.id,
        show_hidden: true
      });

      if (inrow === 6 || inrow === 5) {
        this.page_size = 3*inrow;
      } else if (inrow === 4 || inrow === 3) {
        this.page_size = 4*inrow;
      } else if (inrow === 2 || inrow === 1) {
        this.page_size = 6*inrow;
      }

      if (this.index === 0) {
        this.enable_uploading();
        this.page_size = this.page_size-1;
      }

      let page_size_navi = this.page_size;
      if (this.index == 0) page_size_navi++;

      let max_index = await XHR.get('/content-manager/gallery', {
        command: "max_index",
        album: this.album.id,
        show_hidden: true
      });

      await this.navigation.resize(page_size_navi, max_index);
      console.log("navigation resized");
      await this.load_media();
      console.log("media loaded");
    } catch (e) {
      console.error(e.stack);
    }
  };

  enable_uploading() {
    let add_temp_btn = document.createElement("div");
    add_temp_btn.classList.add("gallery_ui_item");
    add_temp_btn.classList.add("gallery_ui_add");
    add_temp_btn.addEventListener("click", upload_image);

    let grid_ui = this.grid_ui;
    let this_class = this;


    let form = document.createElement('form');
    form.style.display = "none";
    let upload_input = document.createElement("input");
    upload_input.id = "album_file_input";
    upload_input.type = "file";
    upload_input.name = "filei";
    upload_input.multiple = "multiple";
    form.appendChild(upload_input);

    document.body.appendChild(form);


    let submit_func = async function(ev) {
      try {
        console.log("clicked");
        ev.preventDefault();

        display_loading_overlay();
        upload_input.removeEventListener("submit", submit_func);
        var files = this.files;

        console.log("files", files)

        var formData = new FormData(form);

        console.log({ formData: formData, headers: {
            album: this_class.album.title,
            album_id: this_class.album.id
          }});

        console.log("upload_IMAGE");
        progress_block.style.display = "flex";


        let percentage = 0;
        let processing_interval = false;

        progress_block.progress_percentage.innerHTML = "0%";
        progress_block.progress_bar.fill.style.width = "0%";
        var nsrcs = await XHR.post(
          '/content-manager/gallery-upload',
          { 
            formData: formData,
            headers: {
            album: this_class.album.title,
            album_id: this_class.album.id
            },
            on_progress: function(e) {
              if (e.lengthComputable) {
                percentage = (e.loaded / e.total) * 100;
                let percentager = Math.round(percentage * 0.9);
                progress_block.progress_percentage.innerHTML = percentager + "%";
                if (percentage != 100) {
                  progress_block.progress_bar.status.innerHTML = "Uploading media...";
                } else {
                  progress_block.progress_bar.status.innerHTML = "Processing media...";
                  percentage = 90;
                  processing_interval = setInterval(function() {
                    percentage += 1;

                    if (percentage == 99) {
                      progress_block.progress_percentage.innerHTML = percentage+"%";
                      progress_block.progress_bar.fill.style.width = percentage+"%";
                      clearInterval(processing_interval);
                    } else if (percentage < 99) {
                      progress_block.progress_percentage.innerHTML = percentage+"%";
                      progress_block.progress_bar.fill.style.width = percentage+"%";
                    }
                  }, 5000);
                }
                progress_block.progress_bar.fill.style.width = percentager+"%";
              }
            }
          },
          'access_token'
        );
        if (processing_interval) clearInterval(processing_interval);

        progress_block.progress_percentage.innerHTML = "100%";
        progress_block.progress_bar.fill.style.width = "100%";
        progress_block.progress_bar.status.innerHTML = "Done!";
        await this_class.resize(this_class.grid_ui.items_in_row);

        progress_block.style.display = "";
        hide_loading_overlay();
      } catch (e) {
        console.error(e.stack);
      }
    }

    upload_input.addEventListener("change", submit_func, false);

    function upload_image() {
      //upload_input.dispatchEvent(new Event("input"))
      //upload_input.select();
      upload_input.click();
    }

    let text = document.createElement("label");
    text.for = "album_file_input";
    text.innerHTML = "++";
    add_temp_btn.appendChild(text);

    grid_ui.add(add_temp_btn);
  }

  async load_media() {
    try {

      let min = Math.max(0, this.max_index - (this.index*this.page_size+this.page_size))+1;
      let max = this.max_index - this.index*this.page_size;
      if (this.index !== 0) { 
        if (min != 1) min += 1;
        max += 1; 
      };

      let offset = Math.max(0, this.max_index - (this.index*this.page_size+this.page_size-1));
      let limit = this.page_size;
      if (offset == 0 && this.index != 0) {
        limit = (this.max_index%this.page_size)+1;
      } else if (this.index == 0) {
        offset--;
      }

      console.log(this.max_index);
      console.log("PAGE", offset, limit);
      console.log("index", this.index);

      if (offset < 0) offset = 0;

      let media_items = await XHR.get('/content-manager/gallery', {
        command: "page",
        offset: offset,
        limit: limit,
        album: this.album.id,
        show_hidden: true
      });
      console.log(media_items);
      let srcs = [];
      for (let i = 0; i < media_items.length; i++) {
        let item = media_items[i];
        srcs.push(item.src);
      }

      for (var i = 0; i < media_items.length; i++) {
        var src = media_items[i];
        src.album = this.album;
        var image = await Image.init(src, this.grid_ui, select_all, this.images, {
          max_index: max,
          page_index: this.index,
          album: this.album
        }, this.navigation);

        if (image) {
          this.images.push(image);
          this.grid_ui.add(image.element);
        }
        this.grid_ui.resize(window.innerWidth);
      }

      for (var i = 0; i < this.images.length; i++) {
        let image = this.images[i];

        if (image.vid) {
          if (isMobile()) {
            image.vid.currentTime = "0.1";
          } else {
            image.vid.play();
          }
        }
      }
    } catch (e) {
      console.error(e.stack);
    }
  }
  
  checkScroll() {
    if (!isMobile()) {
      const fraction = 0.8;
      for(var i = 0; i < this.images.length; i++) {
        var image = this.images[i];
        if (image.vid) {
          var video = image.vid;
          var vpo = video.getBoundingClientRect();
          var x = vpo.left, y = vpo.top, r = vpo.right, b = vpo.bottom,
          w = video.offsetWidth, h = video.offsetHeight,
          visibleX, visibleY, visible;

          visibleX = Math.max(0, Math.min(w, window.innerWidth - x, r));
          visibleY = Math.max(0, Math.min(h, window.innerHeight - y, b));

          visible = visibleX * visibleY / (w * h);

          if (visible > fraction) {
            video.play();
          } else {
            video.pause();
          }
        }
      }
    } 
  }
  
  destroy() {
    this.delete_selection_button.removeEventListener('click', this.delete_listener);
    window.removeEventListener('resize', this.resize_listener);

    this.images.forEach(function(imag) {
      imag.destroy();
    });
    this.grid_ui.remove_all();
    this.images = [];
//    document.body.removeChild(this.grid_ui.element);
    console.log("DESTROY media page");
  }
}

    function isMobile() {
      return window.matchMedia("only screen and (max-width: 760px)").matches;
    }


