

import album_html from './album.html';

import XHR from 'core/utils/xhr_async.js';



import reflex from 'core/flex-grid/index.js';


import nalbum_html from './nalbum.html';


(async () => {
  try {

/*    import ContentNavigator from 'core/content_navigator/index.js';
    import MediaPage from './media_page.js';*/

//  CATEGORIES +++++++++++

    let category_block = document.getElementById("category_block");
    let categories = await XHR.get('/content-manager/gallery', {
      command: "all_categories"
    });


    let incateg_div = document.createElement("div");
    incateg_div.classList.add("categ_div");
    let incateg = document.createElement("input");
    incateg.type = "text";
    incateg_div.appendChild(incateg);
    let incateg_submit = document.createElement("button");
    incateg_submit.innerHTML = "Add";
    incateg_submit.classList.add("add_categ");
    incateg_submit.addEventListener("click", async function(e) { 
      try {
        let ncateg_res = await XHR.post('/content-manager/gallery', {
          command: "add_category",
          name: incateg.value
        }, "access_token");
        if (ncateg_res.err) {
          alert(ncateg_res.err);
        } else {
          categories.push({ name: incateg.value });
          add_category(incateg.value);
        }
      } catch (err) {
        console.log(err.stack);
      }
    });

    function add_category(name) {
      let categ = document.createElement("div");
      categ.classList.add("categ_div");

      let cname_div = document.createElement("div");
      cname_div.innerHTML = name;
      categ.appendChild(cname_div);

      incateg.value = "";

      let del_categ = document.createElement("span");
      del_categ.classList.add("noselect");
      del_categ.innerHTML = "x";
      del_categ.addEventListener("click", async function(e) { 
        try {
          if (confirm("Are you sure you want to delete this category?")) {
            let ncateg_res = await XHR.post('/content-manager/gallery', {
              command: "del_category",
              name: name
            }, "access_token");
            category_block.removeChild(categ);
            categories.splice(categories.indexOf(name), 1);
          }
        } catch (err) {
          console.log(err.stack);
        }
      });
      categ.appendChild(del_categ);
      category_block.insertBefore(categ, incateg_div);
    }
    incateg_div.appendChild(incateg_submit);

    category_block.appendChild(incateg_div);

    for (let c = 0; c < categories.length; c++) {
      add_category(categories[c].name);
    }



//  ----------------------

//    import ealbum_html from './ealbum.html';


    let albums = document.getElementById("albums");
    let adiv = albums.parentNode;
    let awrap = adiv.parentNode;
    reflex(albums);
    let fspacer = document.createElement('div');
    fspacer.innerHMTL = " ";
    let nalbum = document.getElementById("nalbum");


    window.addEventListener("resize", function(e) {
      reflex(albums);
    });

    let firstup = false;

    let all_albums = await XHR.get("/content-manager/gallery", { command: "all_albums" });

    all_albums.sort((a, b) => a.title.localeCompare(b.title))

    for (let a = 0; a < all_albums.length; a++) {
      add_elem(all_albums[a]);
    }

    function limit_chars(str, limit) {
      if (str.length > limit) {
        str = str.substring(0, limit)+"...";
      }
      return str;
    }


    function add_elem(adata) {
      let naitem = document.createElement("div");
      albums.appendChild(naitem);

      naitem.innerHTML = album_html;


      let nailink = naitem.querySelector("a");
      nailink.href = cms_config.path+"/AlbumEditor?id="+adata.id;
      
      let naititle = naitem.querySelector("h2");
      let album_title = adata.title;
      const max_title_chars = 19;
      naititle.innerHTML = limit_chars(album_title, max_title_chars);

      let naiimg = naitem.querySelector("img");
      naiimg.src = adata.cover ? "/gallery-directory/"+encodeURIComponent(adata.title)+"/"+adata.cover : cms_config.path+"/g/icons/album128.png";
      naiimg.addEventListener("error", function(e) {
        naiimg.src = cms_config.path+"/g/icons/album128.png";
      });

      let naidesc = naitem.querySelector("p");
      let album_description = adata.description || "";
      const max_desc_chars = 30;
      const max_desc_chars_over = 300;
      naidesc.innerHTML = limit_chars(album_description, max_desc_chars);

      naitem.addEventListener('mouseover', e => {
        naidesc.innerHTML = limit_chars(album_description, max_desc_chars_over);
      });

      naitem.addEventListener('mouseleave', e => {
        console.log("LEAVE");
        naidesc.innerHTML = limit_chars(album_description, max_desc_chars);
      });

   /*   naitem.addEventListener("click", async function(e) { 
        await AlbumUI.construct(awrap, adata, categories, function(e) {
          awrap.innerHTML = "";
          awrap.appendChild(adiv);
        }, function(new_title, new_desc) {
          naititle.innerHTML = limit_chars(new_title, max_title_chars);
          naidesc.innerHTML = limit_chars(new_desc, max_desc_chars)
        });
      });*/
      reflex(albums);
    }

    function new_album(e) {
      if (!firstup) {
        let na_popup = document.createElement("div");
        na_popup.classList.add("new_album_popup");
        na_popup.innerHTML = nalbum_html;
        document.body.appendChild(na_popup);

//        nalbum.removeEventListener("click", new_album);
//        nalbum.classList.add("nalbum_active");

//        nalbum.innerHTML = nalbum_html;

        let titlei = na_popup.querySelector("input");
        titlei.focus();
        let desci = na_popup.querySelector("textarea");

        let nasubmit = na_popup.querySelectorAll("button")[1];
        nasubmit.addEventListener("click", async function(e) {
          try {
            if (titlei.value.length < 1) {
              alert("Please insert albums title...");
            } else {
              let response = await XHR.post("/content-manager/gallery", {
                command: "new_album",
                title: titlei.value,
                desc: desci.value
              }, "access_token");
              add_elem({
                id: response,
                title: titlei.value,
                description: desci.value
              });

              window.location.href = cms_config.path+"/AlbumEditor?id="+response;
            }
          } catch (e) {
            console.error(e.stack);
          }
        });

        let nacancel = na_popup.querySelector("button");
        nacancel.addEventListener("click", function(e) {
          document.body.removeChild(na_popup);
          console.log("cancel");
//
        });
      } else {
        firstup = false;
      }
/*

      let ndiv = document.createElement('div');
      ndiv.innerHTML = "n"
      albums.appendChild(ndiv);
      reflex(albums);*/
    }
    
    nalbum.addEventListener("click", new_album);

 
    hide_loading_overlay();
  } catch (e) {
    console.error(e.stack);
  }
})();

