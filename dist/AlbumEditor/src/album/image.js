


import XHR from 'core/utils/xhr_async.js';
import responsive from 'core/utils/responsive.js';

import display_html from './display.html';
import controls_html from './image.html';

import CodeMirror from 'core/codemirror.ui/index.js';

export default class {
  static async init(obj, grid_ui, select_all, images, cfg, navigator) {
    try {
      let src = "/gallery-directory/"+obj.album.title+"/"+obj.name;
      obj.src = src;
      return await new Promise(resolve => { 
        if (src.endsWith(".mp4")) {
          let vid = document.createElement('video');
          vid.innerHTML = 'Your browser does not support HTML5 video.';
          vid.muted = 'muted';
          
          let progress_func = function(e) {
            resolve(new module.exports(vid, obj, grid_ui, select_all, images, cfg, navigator));
            vid.removeEventListener('progress', progress_func);
          };

          vid.addEventListener('progress', progress_func, false); 
          
          vid.addEventListener('ended', function(e) {
            this.currentTime = 0.1;
            this.play();
          }, false);

          if ("" !== ( vid.canPlayType( 'video/mp4; codecs="avc1.42E01E"' )
          || vid.canPlayType( 'video/mp4; codecs="avc1.42E01E, mp4a.40.2"' ) )) {
            let vid_src_mp4 = document.createElement('source');
            vid_src_mp4.type = "video/mp4";
            vid_src_mp4.src =  src;
            vid.appendChild(vid_src_mp4);
          } else if ("" !== vid.canPlayType( 'video/webm; codecs="vp8, vorbis"' )) {
            let vid_src_webm = document.createElement('source');
            vid_src_webm.type = "video/webm";
            vid_src_webm.src =  src.slice(0, -3)+'webm';
            vid.appendChild(vid_src_webm);
          }

          
        } else {
          var img = document.createElement('img');
          img.src = src;
          img.addEventListener("load", e => {
            resolve(new module.exports(img, obj, grid_ui, select_all, images, cfg, navigator));
          });

          img.addEventListener("error", e => {
            console.error(e);
            resolve(undefined);
          });
        }
      });
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  constructor(img, obj, grid_ui, select_all, images, cfg, content_navigator) {
    this.obj = obj;
    this.id = obj.id;
    this.index = obj.index;
    let src = obj.src;

    this.album = obj.album;
    this.album_obj = cfg.album;

    this.navigator = content_navigator;

    this.touchscreen = responsive.is_touch();

    this.images = images;
    let element = this.element = document.createElement("div");
    this.element.classList.add("gallery_ui_item", "noselect");
    this.element.innerHTML = controls_html;
    this.element.appendChild(img);

    if (obj.hidden) {
      let hidden_div = this.hidden_div = document.createElement("div");
      hidden_div.classList.add("hidden_div");
      hidden_div.innerHTML = "HIDDEN";
      this.element.appendChild(hidden_div);
    } else {
      this.hidden_div = false;
    }

    this.mouse_div = document.getElementById('mouse_div');
    
    if (img.tagName === "VIDEO") {
      this.vid = img;
    }
    let this_class = this;
    let _this = this;
    this_class.roll_interval = {};

    
    let controls = this.controls = this.element.querySelector(".image_controls"); 
    let pindex = this.pindex = controls.querySelector(".page_index");

    let header = controls.querySelector(".header");

    let title = this.title = this.element.querySelector(".title");
    if (obj.title) title.innerHTML = obj.title; 
    let etitle = this.etitle = this.element.querySelector(".etitle");
    function edit_title(ev) {
      this_class.keep_controls = true;
      let title_input = document.createElement("input");
      title_input.type = "text";
      title_input.value = title.innerHTML;
      header.replaceChild(title_input, title);
      async function submit_title(ev) {
        try {
          if (ev.type === "keydown" && ev.keyCode == 13 || ev.type === "click") {
            if (title.innerHTML != title_input.value) {
              await XHR.post("/content-manager/gallery", {
                command: "title",
                title: title_input.value,
                id: this_class.id
              }, 'access_token');
              title.innerHTML = title_input.value;
            }
            header.replaceChild(etitle, esubmit);
            header.removeChild(ecancel);
            header.replaceChild(title, title_input);
            this_class.keep_controls = false;
          }
        } catch (e) {
          console.error(e.stack);
        }
      }

      let esubmit = document.createElement("button");
      esubmit.innerHTML = "&#xF105;";
      esubmit.style.paddingLeft = "5px";
      esubmit.style.right = "54px";
      esubmit.classList.add("checro");
      esubmit.addEventListener("click", submit_title);
      header.replaceChild(esubmit, etitle);

      
      let ecancel = document.createElement("button");
      ecancel.innerHTML = "&#xF104;";
      ecancel.style.paddingLeft = "5px";
      ecancel.style.right = "29px";
      ecancel.classList.add("checro");
      ecancel.addEventListener("click", cancel_editing);
      header.appendChild(ecancel);

      function cancel_editing(ev) {
        if (ev.type === "keydown" && ev.keyCode == 27 || ev.type === "click") {
          header.replaceChild(etitle, esubmit);
          header.removeChild(ecancel);
          header.replaceChild(title, title_input);
          this_class.keep_controls = false;
        }
      }

      title_input.addEventListener("keydown", submit_title);
      title_input.addEventListener("keydown", cancel_editing);
    }
    etitle.addEventListener("click", edit_title);


    let bottomline = controls.querySelector(".bottomline");

    let description = this.description = this.element.querySelector(".description");
    if (obj.text) description.innerHTML = obj.text; 
    let edescription = this.edescription = this.element.querySelector(".edescription");
    function edit_description(ev) {
      this_class.keep_controls = true;
      let description_input = document.createElement("input");
      description_input.type = "text";
      description_input.value = description.innerHTML;
      bottomline.replaceChild(description_input, description);
      async function submit_description(ev) {
        try {
          if (ev.type === "keydown" && ev.keyCode == 13 || ev.type === "click") {
            if (description.innerHTML != description_input.value) {
              await XHR.post("/content-manager/gallery", {
                command: "text",
                text: description_input.value,
                id: this_class.id
              }, 'access_token');
              description.innerHTML = description_input.value;
            }
            bottomline.replaceChild(edescription, esubmit);
            bottomline.replaceChild(ejson, ecancel);
            bottomline.replaceChild(description, description_input);
            this_class.keep_controls = false;
          }
        } catch (e) {
          console.error(e.stack);
        }
      }

      let esubmit = document.createElement("button");
      esubmit.innerHTML = "&#xF105;";
      esubmit.style.paddingLeft = "5px";
      esubmit.style.right = "28px";
      esubmit.classList.add("checro");
      esubmit.addEventListener("click", submit_description);
      bottomline.replaceChild(esubmit, edescription);

      
      let ecancel = document.createElement("button");
      ecancel.innerHTML = "&#xF104;";
      ecancel.style.paddingLeft = "5px";
      ecancel.style.right = "3px";
      ecancel.classList.add("checro");
      ecancel.addEventListener("click", cancel_editing);
      bottomline.replaceChild(ecancel, ejson);
  //    bottomline.appendChild(ecancel);

      function cancel_editing(ev) {
        if (ev.type === "keydown" && ev.keyCode == 27 || ev.type === "click") {
          bottomline.replaceChild(edescription, esubmit);
          bottomline.replaceChild(ejson, ecancel);
     //     bottomline.removeChild(ecancel);
          bottomline.replaceChild(description, description_input);
          this_class.keep_controls = false;
        }
      }

      description_input.addEventListener("keydown", submit_description);
      description_input.addEventListener("keydown", cancel_editing);
    }
    edescription.addEventListener("click", edit_description);

    //EDIT JSON
    
    let ejson = this.ejson = this.element.querySelector(".ejson");
    let json_edit = this.element.querySelector(".gallery_json_editor");
    let json_edit_block = json_edit.querySelector(".json_editor_cm");
    let html_editor = new CodeMirror(JSON.stringify(obj.json_data), 'js', false);
    json_edit_block.appendChild(html_editor.element);
    html_editor.cm.setSize("100%", "100%");
    html_editor.cm.refresh();
    let checkbox_before_ejson = false;

    let acover_ck = json_edit.querySelector(".props_div input[type=checkbox].acover_ck");
    acover_ck.checked = obj.src == obj.album.cover ? true : false;

    let hidden_ck = json_edit.querySelector(".props_div input[type=checkbox].hidden_ck");
    hidden_ck.checked = obj.hidden;

    function edit_json(e) {
      json_edit.style.display = "";
      html_editor.cm.refresh();
      if (checkbox.checked) checkbox_before_ejson = true;
      checkbox.checked = true;
    }
    
    ejson.addEventListener("click", edit_json);

    let ejson_save = json_edit.querySelector(".ejson_save");
    ejson_save.addEventListener("click", async function(e) {
      try {
        JSON.parse(html_editor.cm.getValue());
      } catch (jsone) {
        alert("Your JSON string did not pass validation!");
        console.error(jsone.stack);
        return false;
      }

      let cover_fname = obj.src.split('/');
      cover_fname = cover_fname[cover_fname.length-1];
      await XHR.post("/content-manager/gallery", {
        command: "json_edit",
        hidden: hidden_ck.checked,
        album_cover: acover_ck.checked ? {
          id: obj.album.id,
          src: cover_fname 
        } : false,
        json: html_editor.cm.getValue(),
        id: this_class.id
      }, "access_token");

      if (acover_ck.checked) {
        _this.album.cover = cover_fname;
        _this.album.set_cover_src();
      }


      if (hidden_ck.checked && !_this.hidden_div) {
        let hidden_div = _this.hidden_div = document.createElement("div");
        hidden_div.classList.add("hidden_div");
        hidden_div.innerHTML = "HIDDEN";
        _this.element.appendChild(hidden_div);
      } else if (_this.hidden_div && !hidden_ck.checked) {
        _this.element.removeChild(_this.hidden_div);
        _this.hidden_div = false;
      }

      json_edit.style.display = "none";
      if (!checkbox_before_ejson) {
        checkbox.checked = false;
      } else {
        checkbox_before_ejson = false;
      }
    });

    let ejson_cancel = json_edit.querySelector(".ejson_cancel");
    ejson_cancel.addEventListener("click", function(e) {
      json_edit.style.display = "none";
      if (!checkbox_before_ejson) {
        checkbox.checked = false;
      } else {
        checkbox_before_ejson = false;
      }
    });

    this.element.removeChild(controls);

    this.move_this = function(event) {
      content_navigator.drag_mode();
      this_class.mouse_div.style.display = "block";
      let pos = offset(this_class.element);
      pos.x = pos.left - document.documentElement.scrollLeft;
      pos.y = pos.top - document.documentElement.scrollTop;

      this_class.element.style.position = "fixed";
      this_class.element.style.zIndex = 10;

      this_class.startX = event.clientX - pos.x;
      this_class.startY = event.clientY - pos.y;

      this_class.element.style.left = pos.x + 'px';
      this_class.element.style.top = pos.y + 'px';

      document.documentElement.addEventListener('mousemove', drag);
      this_class.mouse_div.style.display = "block";

      this_class.mouse_div.addEventListener('mouseup', stopDrag);

      function drag(e) {

        for (let i = 0; i < images.length; i++) {
          let image = images[i];
          if (image != this_class) {
            let img_pos = offset(image.element);
            img_pos.x = img_pos.left - document.documentElement.scrollLeft;
            img_pos.y = img_pos.top - document.documentElement.scrollTop;
            let img_rect = {
              left: img_pos.x, right: img_pos.x + this_class.element.offsetWidth,
              top: img_pos.y, bottom: img_pos.y + this_class.element.offsetHeight,
              mouse_over: function(img) {
                grid_ui.element.style.marginBottom = grid_ui.item_a+"px";
                const pos_index = images.indexOf(img);
                grid_ui.remove(this_class.element);
                images.splice(images.indexOf(this_class), 1);

                if (cfg.page_index === 0) {
                  grid_ui.insert(this_class.element, pos_index+1);
                } else {
                  grid_ui.insert(this_class.element, pos_index);
                }
                images.splice(pos_index, 0, this_class);
                grid_ui.element.style.marginBottom = "";
              }
            }

            if (
              img_rect.left < e.clientX && e.clientX < img_rect.right &&
              img_rect.top < e.clientY && e.clientY < img_rect.bottom
            ) {
              img_rect.mouse_over(image); 
            }
          }
        }

        for (let b = 0; b < content_navigator.drag_buttons.length; b++) {
          let button = content_navigator.drag_buttons[b];
          for (let n = 0; n < 2; n++) {
            let which = "top";
            if (n == 1) which = "bottom";

            let img_pos = offset(button[which]);
            img_pos.x = img_pos.left - document.documentElement.scrollLeft;
            img_pos.y = img_pos.top - document.documentElement.scrollTop;
            let img_rect = {
              left: img_pos.x, right: img_pos.x + button[which].offsetWidth,
              top: img_pos.y, bottom: img_pos.y + button[which].offsetHeight,
              mouse_over: function(button) {
                button.classList.add("hover");
              },
              mouse_out: function(button) {
                button.classList.remove("hover");
              }
            }

            if (
              img_rect.left < e.clientX && e.clientX < img_rect.right &&
              img_rect.top < e.clientY && e.clientY < img_rect.bottom
            ) {
              img_rect.mouse_over(button[which]); 
            } else {
              img_rect.mouse_out(button[which]);
            }

          }
        }

        if (content_navigator.drag_buttons.length > 1) {
          for (let r = 0; r < 2; r++) {
            let side = "left";
            if (r > 0) side = "right";
            if (r == 1) r = content_navigator.drag_buttons.length-1;
            console.log("r", r);
            let button = content_navigator.drag_buttons[r];
            console.log("button", button);
            for (let n = 0; n < 2; n++) {
              let which = "top";
              if (n == 1) which = "bottom";

              let img_pos = offset(button[which]);
              img_pos.x = img_pos.left - document.documentElement.scrollLeft;
              img_pos.y = img_pos.top - document.documentElement.scrollTop;

              let img_rect = {
                left: img_pos.x, right: img_pos.x + button[which].offsetWidth,
                top: img_pos.y, bottom: img_pos.y + button[which].offsetHeight,
                mouse_over: function(button) {
                  if (!this_class.roll_interval[which]) {
                    this_class.roll_interval[which] = {};
                  }

                  if (!this_class.roll_interval[which][side]) {
                    this_class.roll_interval[which][side] = setInterval(function() {
                      if (r > 0) {
                        console.log("roll_right");
                        content_navigator.roll_right();
                      } else {
                        console.log("roll_left");
                        content_navigator.roll_left();
                      }
                    }, 250);
                  }
                },
                mouse_out: function(button) {
                  if (this_class.roll_interval[which]) {
                    clearInterval(this_class.roll_interval[which][side]);
                    this_class.roll_interval[which][side] = false;
                  }
                }
              }
              let roll_area_width = 100;
              if (r > 0) {
                img_rect.left += button[which].offsetWidth;
                img_rect.right += 2*button[which].offsetWidth;

                img_rect.right += roll_area_width;
              } else {
                img_rect.left -= roll_area_width;
              }

              if (
                img_rect.left < e.clientX && e.clientX < img_rect.right &&
                img_rect.top < e.clientY && e.clientY < img_rect.bottom
              ) {
                img_rect.mouse_over(button[which]); 
              } else {
                img_rect.mouse_out(button[which]);
              }
            }
          }
        }


        var eClientX = e.clientX;
        var eClientY = e.clientY;

        var nX = (eClientX - this_class.startX);
 /*       var nX2 = nX + this_class.element.offsetWidth;

        if (nX < 0) nX = 0;
        if (nX2 > window.innerWidth) {
          nX = window.innerWidth - this_class.element.offsetWidth;
        }*/

        this_class.element.style.left = nX + 'px';

        let nY = (eClientY - this_class.startY);
/*        var nY2 = nY + this_class.element.offsetHeight;

        if (nY < 0) nY = 0;
        if (nY2 > window.innerHeight) {
          nY = window.innerHeight - this_class.element.offsetHeight;
        }*/
        

        let fbtn = content_navigator.drag_controls.top;
        let button_bottom = offset(fbtn).top - document.documentElement.scrollTop + fbtn.offsetHeight;
        if (button_bottom > nY) {
          nY = button_bottom;      
        }
        
        let bfbtn = content_navigator.drag_controls.bottom;
        let button_top = offset(bfbtn).top - document.documentElement.scrollTop - grid_ui.item_a - grid_ui.padding;
        console.log(button_top);
        if (button_top < nY) {
          nY = button_top;      
        }

        this_class.element.style.top = nY + 'px';

      }

      async function stopDrag(e) {
        try {
          this_class.mouse_div.style.display = "none";
          this_class.mouse_div.removeEventListener('mouseup', stopDrag);

          let moved = false;

          for (let b = 0; b < content_navigator.drag_buttons.length; b++) {
            let button = content_navigator.drag_buttons[b];
            for (let n = 0; n < 2; n++) {
              let which = "top";
              if (n == 1) which = "bottom";

              for (let r = 0; r < 2; r++) {
                let side = "left";
                if (r > 0) side = "right";
              
                if (this_class.roll_interval[which]) {
                  clearInterval(this_class.roll_interval[which][side]);
                  this_class.roll_interval[which][side] = false;
                }
              }

              let img_pos = offset(button[which]);
              img_pos.x = img_pos.left - document.documentElement.scrollLeft;
              img_pos.y = img_pos.top - document.documentElement.scrollTop;
              let img_rect = {
                left: img_pos.x, right: img_pos.x + button[which].offsetWidth,
                top: img_pos.y, bottom: img_pos.y + button[which].offsetHeight,
                drop: async function(button) {
                  try {
                    display_loading_overlay();
                    let newi = content_navigator.max_index-(button.innerHTML-1)*content_navigator.page_size+1;
                    if (newi < 1) newi = 1; else if (newi > content_navigator.max_index) newi = content_navigator.max_index;
                    let oldi = this_class.index;
                    
                    if (newi < oldi) newi -= content_navigator.page_size-1;
                    if (newi < 1) newi = 1; else if (newi > content_navigator.max_index) newi = content_navigator.max_index;

                    console.log("drop", newi, oldi, content_navigator.max_index,"-",button.innerHTML,"*",content_navigator.page_size,"+",2);
                    if (newi !== oldi) {
                      console.log("NEW INDEX", newi, oldi);
                      const resp = await XHR.post("/content-manager/gallery", {
                        command: "new_index",
                        new_index: newi,
                        old_index: oldi,
                        id: this_class.id,
                        aid: this_class.album.id
                      }, 'access_token');

                      moved = true;
                      console.log(resp, content_navigator.cur_page, "<< here");
                      await content_navigator.get_page(content_navigator.cur_page.index, content_navigator.page_size);
                    }
                    hide_loading_overlay();
                  } catch (err) {
                    console.error(err);
                  }
                },
              }

              if (
                img_rect.left < e.clientX && e.clientX < img_rect.right &&
                img_rect.top < e.clientY && e.clientY < img_rect.bottom
              ) {
                img_rect.drop(button[which]); 
              }
            }
          }

          document.documentElement.removeEventListener('mousemove', drag);
          const inpage_id = images.indexOf(this_class);
          const newi = cfg.max_index-inpage_id, oldi = this_class.index;
          console.log("nindex", newi, "oindex", oldi, e.clientX);

          if (newi !== oldi && !moved) {
            console.log("NEW INDEX", newi, oldi);
            const resp = await XHR.post("/content-manager/gallery", {
              command: "new_index",
              new_index: cfg.max_index-inpage_id,
              old_index: this_class.index,
              id: this_class.id,
              aid: this_class.album.id
            }, 'access_token');

            console.log(resp);
//            await content_navigator.cur_page.resize();

            if (newi < oldi) {
              console.log("id >= "+newi+" AND id < "+oldi, "id++");
              for (let i = 0; i < images.length; i++) {
                let img_id = images[i].index;
                if (img_id >= newi && img_id < oldi) {
                  images[i].index += 1;
                }
              }
            } else {
              console.log("id <= "+newi+" AND id > "+oldi, "id--");
              for (let i = 0; i < images.length; i++) {
                let img_id = images[i].index;
                if (img_id <= newi && img_id > oldi) {
                  images[i].index -= 1;
                }
              }
            }


            if (resp === "success") {
              this_class.index = cfg.max_index-inpage_id;
            }
          }
          this_class.element.style.position = "";
          this_class.element.style.zIndex = "";
          this_class.element.style.left = "";
          this_class.element.style.top = "";

          content_navigator.normal_mode();
        } catch (ex) {
          console.error(ex.stack);
        }
        
      }

    }
    title.addEventListener("mousedown", this.move_this);
    description.addEventListener("mousedown", this.move_this);

    let checkbox = this.checkbox = controls.querySelector('input[type=checkbox]');
    checkbox.type = "checkbox";
    checkbox.addEventListener("change", function(e) {
      if (!checkbox.checked) {
        select_all.checked = false; 
      }
    });

    this.displayed = false;
    this.img = img;

    img.addEventListener('click', function() {
      this_class.display();
    });

    if (this_class.touchscreen) this_class.element.appendChild(controls);

    this.element.addEventListener('mouseover', e => {
      if (!this_class.displayed && !this_class.element.contains(controls) && !this_class.touchscreen) {
        this_class.element.appendChild(controls);
      }
    });

    this.element.addEventListener('mouseleave', e => {
      if (!this_class.displayed) {
        if (!checkbox.checked && !this_class.keep_controls && !this_class.touchscreen) {
          this_class.element.removeChild(controls);
        }
      }
    });

    this.src = src;
    this.filename = src.replace(/^.*[\\\/]/, '');

  }
  
  destroy() {
    this.title.removeEventListener("mousedown", this.move_this);
  }

  display() {
    this.create_display();
    let display_controls = this.display_element.querySelector(".display_controls");
    let back_btn = this.display_element.querySelector('.display_close');
    let left_btn = this.left_btn, right_btn = this.right_btn;


    const this_index = this.images.indexOf(this);
    if (this_index < 1 && this.navigator.cur_page.index == 0) {
      if (display_controls.contains(left_btn)) {
        display_controls.removeChild(left_btn);
      }
    } else {
      if (!display_controls.contains(left_btn)) {
        display_controls.appendChild(left_btn);
      }
      left_btn.addEventListener("click", this.navigate_left);
    }

    console.log(this.navigator.cur_page.index, ">", this.navigator.max_pages);
    if (this_index > this.images.length-2 && this.navigator.cur_page.index == this.navigator.max_pages-1) {
      if (display_controls.contains(right_btn)) {
        display_controls.removeChild(right_btn);
      }
    } else {
      if (!display_controls.contains(right_btn)) {
        display_controls.appendChild(right_btn);
      }
      right_btn.addEventListener("click", this.navigate_right);
    }

    let this_class = this;

    document.body.style.overflow = "hidden";
    document.body.style.position = "fixed";
    this_class.displayed = true;
 
    let hide_after = 2000; // 1000 miliseconds
    let last_moved = Date.now();

    this.interv = setInterval(function() {
      let now = Date.now();
      if (last_moved < now-hide_after) {
        display_controls.style.display = "none";
      }
    }, 1000);

    this.mouse_moved_listener = function () {
      if (display_controls.style.display === "none") {
        display_controls.style.display = "";
      }
      last_moved = Date.now();
    };

    document.addEventListener('mousemove', this.mouse_moved_listener, false);

    back_btn.addEventListener("click", this.hide_element);

    document.body.appendChild(this.display_element);
    if (this.dvid) {
      this.dvid.play();
    }
  }

  create_display() {
    this.display_element = document.createElement("div");
    this.display_element.classList.add("gallery_ui_display");
    this.display_element.classList.add("noselect");
    this.display_element.innerHTML = display_html;
    let dimg = this.display_element.querySelector("img"); 
    let dvid = this.display_element.querySelector("video");
    
    let srcs = [];
    
    if (this.img.tagName === "VIDEO") {
      this.dvid = dvid;
      if ("" !== ( dvid.canPlayType( 'video/mp4; codecs="avc1.42E01E"' )
      || dvid.canPlayType( 'video/mp4; codecs="avc1.42E01E, mp4a.40.2"' ) )) {
        let vid_src_mp4 = document.createElement('source');
        vid_src_mp4.type = "video/mp4";
        vid_src_mp4.src =  this.src;
        srcs.push(vid_src_mp4);
        dvid.appendChild(vid_src_mp4);
      } else if ("" !== dvid.canPlayType( 'video/webm; codecs="vp8, vorbis"' )) {
        let vid_src_webm = document.createElement('source');
        vid_src_webm.type = "video/webm";
        vid_src_webm.src =  this.src.slice(0, -3)+'webm';
        srcs.push(vid_src_webm);
        dvid.appendChild(vid_src_webm);
      }
      dimg.style.display = "none";
    } else {
      dimg.src = this.src;
      dvid.style.display = "none";
    }
        
    let this_class = this;
    
    this.hide_element = function(e) {
      if (this_class.img.tagName === "VIDEO") {
        for (let s = 0; s < srcs.length; s++) {
          srcs[s].src = "";
        }
        dvid.load();
      }

      document.body.style.overflow = "auto";
      document.body.style.position = "";
      this_class.hide(); 
    };

    this.navigate_left = async function(e) {
      const next_index = this_class.images.indexOf(this_class)-1;
      if (next_index > -1 && next_index < this_class.images.length) {
        this_class.images[next_index].display();
        this_class.hide();
      } else {
        this_class.hide();
        await this_class.navigator.select_page(this_class.navigator.cur_page.index-1);
        this_class.navigator.cur_page.images[this_class.navigator.cur_page.images.length-1].display();
      }
    }

    this.navigate_right = async function(e) {
      const next_index = this_class.images.indexOf(this_class)+1;
      if (next_index > -1 && next_index < this_class.images.length) {
        this_class.images[next_index].display();
        this_class.hide();
      } else {
        this_class.hide();
        await this_class.navigator.select_page(this_class.navigator.cur_page.index+1);
        this_class.navigator.cur_page.images[0].display();
      }
    }

    this.left_btn = this.display_element.querySelector('.display_left');
    this.right_btn = this.display_element.querySelector('.display_right');
  }

  hide() { 
    document.body.removeChild(this.display_element);
    this.display_element = false;
    this.displayed = false;
    document.removeEventListener('mousemove', this.mouse_moved_listener, false);
    document.removeEventListener('click', this.hide_element, false);
    document.removeEventListener('click', this.navigate_left, false);
    document.removeEventListener('click', this.navigate_right, false);
    clearInterval(this.interv);
  }

  select() {
    this.checkbox.checked = true;
  }

  deselect() {
    this.checkbox.checked = false;
  }
}

function offset(elem) {
  if(!elem) elem = this;

  var x = elem.offsetLeft;
  var y = elem.offsetTop;

  while (elem = elem.offsetParent) {
      x += elem.offsetLeft;
      y += elem.offsetTop;
  }

  return { left: x, top: y };
}

