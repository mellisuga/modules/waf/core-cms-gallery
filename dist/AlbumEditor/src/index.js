
import XHR from 'core/utils/xhr_async.js';
import AlbumUI from './album/index.js';

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

const album_id = getParameterByName("id");

(async function() {
  try {
    hide_loading_overlay();

    let awrap = document.getElementById("album_container");

    let categories = await XHR.get('/content-manager/gallery', {
      command: "all_categories"
    });

    console.log("AID", album_id);
    let adata = await XHR.get('/content-manager/gallery', {
      command: "album_by_id",
      id: album_id
    });
    console.log("ADATA", adata);

    await AlbumUI.construct(awrap, adata, categories, function(e) {
      window.location.href = cms_config.path;
    }, function(new_title, new_desc) {

    });
  } catch (e) {
    console.error(e.stack);
  }
})();
