
import XHR from 'core/utils/xhr_async.js';
import ContentNavigator from 'core/content_navigator/index.js';
import MediaPage from './media_page.js';

export default class {
  static async construct(element, album_data, categories, on_cancel, on_change) {
    try {
      let _this = new module.exports(element, album_data, categories, on_cancel, on_change);

      let max_index = await XHR.get('/content-manager/gallery', {
        command: "max_index",
        album: album_data.id
      });

      let cur_media_page;
      let navigator = await ContentNavigator.init({
        div: element.querySelector(".content_div"),
        max_index: max_index,
        first_page: -1
      }, async function(index, page_size, max_index, content_navigator) {
         if (index === 0) page_size -= 1;
         if (cur_media_page) cur_media_page.destroy(); 
         let media_page = cur_media_page = await MediaPage.init(
           index, page_size, max_index, content_navigator, album_data 
         );
          media_page.listen_resize();
         await media_page.resize();

         hide_loading_overlay();
         return media_page; // TODO: remove max_pages argument as it is never used nor will be...
       });
       await navigator.get_page(0, navigator.page_size);
      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(element, album_data, categories, on_cancel, on_change) {
    this.element = element;

    let aedit_box = element.querySelector(".aedit-box");
    let adata_box = aedit_box.querySelector("div.aprops_box");
    let acontrols_box = aedit_box.querySelector("div.acontrols_box");

    let buttons = element.querySelectorAll("button");
    let eacancel = element.querySelector(".back_arrow");
    eacancel.addEventListener("click", on_cancel);

    let eatitle = element.querySelector("h2");
    eatitle.innerHTML = album_data.title;

    let eadesc = element.querySelector("p");
    eadesc.innerHTML = album_data.description;

    let eacover = element.querySelector("div.album_cover");
    let eacover_img = eacover.querySelector("img");
    eacover_img.src = album_data.cover ? "/gallery-directory/"+encodeURIComponent(album_data.title)+"/"+album_data.cover : cms_config.path+"/g/icons/album256.png";

    album_data.set_cover_src = function() {
      eacover_img.src = album_data.cover ? "/gallery-directory/"+encodeURIComponent(album_data.title)+"/"+album_data.cover : cms_config.path+"/g/icons/album256.png";
    };
    eacover_img.addEventListener("error", function(e) {
      eacover_img.src = cms_config.path+"/g/icons/album256.png";
    });

    let eacateg = element.querySelector("div.categ_display");
    eacateg.innerHTML = (album_data.categories && album_data.categories.length > 0) ? "Category: "+album_data.categories[0] : "no category";

    let ninput = document.createElement('input');
    let ntextarea = document.createElement("textarea");
    let ncategselect = document.createElement("select");

    let dcateg = document.createElement("option");
    dcateg.innerHTML = "Category";
    dcateg.selected = true;
    dcateg.disabled = true;
    ncategselect.appendChild(dcateg);
    for (let c = 0; c < categories.length; c++) {
      let categ = document.createElement("option");
      categ.innerHTML = categories[c].name;
      categ.value = categories[c].name;
      ncategselect.appendChild(categ);
    }


      console.log(acontrols_box);

// controls ----
    let actl_save = document.createElement("button");
    actl_save.innerHTML = "Save";
    actl_save.addEventListener("click", async function(e) {
      try {
        let response = await XHR.post("/content-manager/gallery", {
          command: "edit_album",
          title: ninput.value,
          desc: ntextarea.value,
          categ: [ncategselect.value],
          aid: album_data.id
        }, "access_token");
        if (!response.err) {
          album_data.title = ninput.value;
          album_data.description = ntextarea.value;
          if (album_data.categories && album_data.categories.length > 0) album_data.categories[0] = ncategselect.value;
          eacateg.innerHTML = (album_data.categories && album_data.categories.length > 0) ? "Category: "+album_data.categories[0] : "no category";
          on_change(ninput.value, ntextarea.value);

          eatitle.innerHTML = ninput.value; 
          eadesc.innerHTML = ntextarea.value; 
          adata_box.replaceChild(eatitle, ninput);
          adata_box.replaceChild(eadesc, ntextarea);
          adata_box.replaceChild(eacateg, ncategselect);
          
          aedit_box.classList.remove("aedit-box-active");

          acontrols_box.innerHTML = "";
          acontrols_box.appendChild(aedit_btn);
        } else {
          console.log("ealbum err:", response.err);
        }
        console.log(response);
      } catch (e) {
        console.error(e.stack);
      }
    });

    let actl_cancel = document.createElement("button");
    actl_cancel.innerHTML = "Cancel";
    actl_cancel.addEventListener("click", function(e) {
      adata_box.replaceChild(eatitle, ninput);
      adata_box.replaceChild(eadesc, ntextarea);
      adata_box.replaceChild(eacateg, ncategselect);
      
      aedit_box.classList.remove("aedit-box-active");

      acontrols_box.innerHTML = "";
      acontrols_box.appendChild(aedit_btn);
    });

    let eadelete = document.createElement("button");
    eadelete.innerHTML = "Delete album";
    eadelete.addEventListener("click", async function(e) {
      try {
        if (confirm('Do you really want to delete this album?')) {
          display_loading_overlay();
          let resp = await XHR.post("/content-manager/gallery", {
            command: "rm_album",
            id: album_data.id
          }, "access_token");
          if (!resp.err) {
            window.location.href = cms_config.path;
          } else {
            console.log("DELETE ERR:", resp.err);
          }
        }
      } catch(e) {
        console.error(e.stack);
      }
    });

    let aedit_btn = aedit_box.querySelector(".edit_btn");
    aedit_btn.addEventListener("click", function(e) {
      ninput.value = album_data.title;
      ntextarea.value = album_data.description;
      if (album_data.categories && album_data.categories.length > 0) ncategselect.value = album_data.categories[0];

      adata_box.replaceChild(ninput, eatitle);
      adata_box.replaceChild(ntextarea, eadesc);
      adata_box.replaceChild(ncategselect, eacateg);
      
      aedit_box.classList.add("aedit-box-active");

      acontrols_box.innerHTML = "";
      acontrols_box.appendChild(actl_save);
      acontrols_box.appendChild(actl_cancel);
      acontrols_box.appendChild(eadelete);
    });



  }
}
